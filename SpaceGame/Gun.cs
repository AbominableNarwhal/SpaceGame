﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Utilities;

namespace SpaceGame
{
    public class Gun
    {
        GunMod primaryMod;
        GunMod secondaryMod;
        List<GunMod> modList;
        Vector2D position;
        Vector2D direction;
        public Gun()
        {
            GunMod mod = new NormMod();
            modList = new List<GunMod>();
            modList.Add(mod);
            primaryMod = mod;
        }
        public bool OnShootPrimary(Vector2D position, Vector2D direction)
        {
            return primaryMod.Shoot(position, direction);
        }
        public bool OnShootSecondary(Vector2D position, Vector2D direction)
        {
            return secondaryMod.Shoot(position, direction);
        }
        public void equipPrimaryMod(int modID)
        { }
        public void equipMod(int modID)
        { }
        public void scrollSecondaryMod()
        { }

    }
}
