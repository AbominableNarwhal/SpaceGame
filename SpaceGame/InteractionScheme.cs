﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceGame
{
    public interface InteractionScheme
    {
        bool IsOnground();
        void SetOnground(bool onGround);
        void MoveLeft();
        void MoveRight();
        void stopHorizantal();
        void Ascend();
        void AscendStop();
        void Shoot();
    }
}
