﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MonoMysterionEngine.Utilities;

namespace SpaceGame
{
    abstract public class GunMod
    {
        protected long timeLeft;
        long coolDown = 500;
        Stopwatch stopwatch;
        bool firstShot;
        public long CurrentCoolDown
        {
            get
            {
                timeLeft = coolDown - stopwatch.ElapsedMilliseconds;
                if (timeLeft <= 0 || firstShot)
                {
                    stopwatch.Restart();
                    firstShot = false;
                    return 0;
                }
                return timeLeft;
            }
        }
        public GunMod()
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            firstShot = true;
        }

        public abstract bool OnShoot(Vector2D position, Vector2D direction);
        public bool Shoot(Vector2D position, Vector2D direction)
        {
            if (CurrentCoolDown == 0)
            {
                return OnShoot(position, direction);
            }
            return false;
        }
    }
}
