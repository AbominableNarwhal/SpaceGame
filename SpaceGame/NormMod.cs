﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine;

namespace SpaceGame
{
    public class NormMod : GunMod
    {
        public override bool OnShoot(Vector2D position, Vector2D direction)
        {
            NormBullet bullet = new NormBullet();
            bullet.Position.vector = position.vector;
            bullet.Velocity.vector = direction.vector;
            MysterionEngineGame.Game.ObjectLocator.AddObject(bullet);
            return true;
        }
    }
}
