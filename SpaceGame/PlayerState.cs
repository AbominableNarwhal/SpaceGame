﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceGame
{
    public class PlayerState
    {
        protected TestObject player;
        public PlayerState(TestObject player)
        {
            this.player = player;
        }
        public virtual void OnEnter(int speed) { }
        public virtual void OnExit(int speed) { }
        public virtual void MoveLeft(int speed) { }
        public virtual void MoveRight(int speed) { }
        public virtual void stopHorizantal() { }
        public virtual void Ascend(int speed) { }
        public virtual void AscendStop() { }
        public virtual void OnEnterArial() { }
        public virtual void OnEnterGround() { }
        public virtual void Shoot() { }
    }

    public class UnboundPlayerState : PlayerState
    {
        public UnboundPlayerState(TestObject player) : base(player)
        { }
        public override void OnEnter(int speed)
        { }
        public override void OnExit(int speed) { }
        public override void MoveLeft(int speed)
        {
            player.Velocity.vector.X = -speed;
        }
        public override void MoveRight(int speed)
        {
            player.Velocity.vector.X = speed;
        }
        public override void stopHorizantal()
        {
            player.Velocity.vector.X = 0;
        }
        public override void Ascend(int speed)
        {
            if (player.IsOnground())
            {
                player.Velocity.vector.Y = -2000;
            }
            else
            {
                player.JetPackPush.vector.Y = -6000;
            }
        }
        public override void AscendStop()
        {
            if (!player.IsOnground())
            {
                player.JetPackPush.vector.Y = 0;
            }
        }
        public override void OnEnterArial() { }
        public override void OnEnterGround()
        {
            player.JetPackPush.vector.Y = 0;
        }
        public override void Shoot()
        {
            player.PlayerGun.OnShootPrimary(player.Position, player.Position);
        }
    }
}
