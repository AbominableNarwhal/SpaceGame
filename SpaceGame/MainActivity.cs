﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine;

namespace SpaceGame
{
    public class MainActivity : Activity
    {
        TestObject testObject;
        Platform platform;
        public override void Start()
        {
            testObject = new TestObject();
            MysterionEngineGame.Game.ObjectLocator.AddObject(testObject);
            platform = new Platform();
            MysterionEngineGame.Game.ObjectLocator.AddObject(platform);
        }
    }
}
