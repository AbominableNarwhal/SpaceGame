﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Components;

namespace SpaceGame
{
    class TestEnemy : GameObject, CollisionHandler
    {
        Sprite sprite;
        GameBox gameBox;
        BasicRenderComponent renderComponent;
        BasicCollisionComponent collisionComponent;
        public TestEnemy()
        {
            //inicialize class variables
            sprite = new Sprite();
            sprite.LoadImage("enemy_thing");
            Position.vector.X = 500;
            //sprite.SetScale(0.25f, 0.25f);
            gameBox = new GameBox(0, 0, sprite.Width, sprite.Height, Position);
            //set up components
            renderComponent = new BasicRenderComponent(sprite, Position);
            AddComponent(renderComponent);
            collisionComponent = new BasicCollisionComponent(this, this);
            collisionComponent.BoundingBoxes.Add(gameBox);
            AddComponent(collisionComponent);
        }
        public void OnCollisionEnter(GameObject other, GameBox mine, GameBox otherBox)
        {
        }
        public void OnCollisionExit(GameObject other, GameBox mine, GameBox otherBox)
        {
        }
    }
}
