﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Components;
using Microsoft.Xna.Framework.Input;

namespace SpaceGame
{
    public class TestObject : GameObject, InteractionScheme
    {
        Gun gun;
        PlayerState playerState;
        Vector2D jetPackPush;
        Sprite sprite;
        GameBox gameBox;
        BasicRenderComponent renderComponent;
        GravityMovementComponent movementComponent;
        PlayerInputComponent inputComponent;
        CollisionComponent collisionComponent;
        bool onGround;
        int speed = 260;
        public TestObject()
        {
            //initialize class variables
            gun = new Gun();
            playerState = new UnboundPlayerState(this);
            jetPackPush = new Vector2D();
            onGround = false;
            sprite = new Sprite();
            sprite.LoadImage("little_hero");
            //sprite.SetScale(0.25f, 0.25f);
            gameBox = new GameBox(0, 0, sprite.Width, sprite.Height, Position);
            //set up components
            renderComponent = new BasicRenderComponent(sprite, Position);
            AddComponent(renderComponent);
            movementComponent = new GravityMovementComponent(this, Position, Velocity, Acceleration, jetPackPush);
            AddComponent(movementComponent);
            inputComponent = new PlayerInputComponent(this);
            AddComponent(inputComponent);
            collisionComponent = new CollisionComponent(this);
            collisionComponent.BoundingBoxes.Add(gameBox);
            AddComponent(collisionComponent);
        }
        public Gun PlayerGun
        {
            get { return gun; }
        }
        public Vector2D JetPackPush
        {
            get { return jetPackPush; }
        }
        public bool IsOnground()
        {
            return onGround;
        }
        public void SetOnground(bool onGround)
        {
            if (this.onGround != onGround)
            {
                if (onGround)
                {
                    playerState.OnEnterGround();
                }
                else
                {
                    playerState.OnEnterArial();
                }
                this.onGround = onGround;
            }
        }
        public void MoveLeft()
        {
            playerState.MoveLeft(speed);
        }
        public void MoveRight()
        {
            playerState.MoveRight(speed);
        }
        public void stopHorizantal()
        {
            playerState.stopHorizantal();
        }
        public void Ascend()
        {
            playerState.Ascend(speed);
        }
        public void AscendStop()
        {
            playerState.AscendStop();
        }
        public void Shoot()
        {
            playerState.Shoot();
        }
    }
}
