﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Components;

namespace SpaceGame
{
    public class Platform : GameObject, CollisionHandler
    {
        Sprite sprite;
        GameBox gameBox;
        BasicRenderComponent renderComponent;
        BasicCollisionComponent collisionComponent;
        public Platform()
        {
            //inicialize class variables
            sprite = new Sprite();
            sprite.LoadImage("platform");
            Position.vector.Y = 720 - sprite.Height;
            gameBox = new GameBox(0, 0, sprite.Width, sprite.Height, Position);
            //set up components
            renderComponent = new BasicRenderComponent(sprite, Position);
            AddComponent(renderComponent);
            collisionComponent = new BasicCollisionComponent(this, this);
            collisionComponent.BoundingBoxes.Add(gameBox);
            AddComponent(collisionComponent);
        }
        public void OnCollisionEnter(GameObject other, GameBox mine, GameBox otherBox)
        {
            if (other is TestObject)
            {
                Console.WriteLine("Enter Collision?");
                TestObject testObject = (TestObject)other;
                Rectangle intersect = Rectangle.Intersect(mine.BoundingBox.getUnrotatedRectangle(), otherBox.BoundingBox.getUnrotatedRectangle());
                if (intersect.Height <= intersect.Width)
                {
                    testObject.Position.vector.Y -= intersect.Height;
                    testObject.Velocity.vector.Y = 0;
                    testObject.SetOnground(true);
                }
            }
        }
        public void OnCollisionExit(GameObject other, GameBox mine, GameBox otherBox)
        {
            if (other is TestObject)
            {
                Console.WriteLine("Exit Collision?");
                TestObject testObject = (TestObject)other;
                testObject.SetOnground(false);
            }
        }
    }
}
