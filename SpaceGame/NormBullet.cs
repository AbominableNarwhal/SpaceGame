﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Components;

namespace SpaceGame
{
    public class NormBullet : GameObject
    {
        Sprite sprite;
        BasicRenderComponent renderComponent;
        MovementComponent movementComponent;
        public NormBullet()
        {
            //Utilities
            sprite = new Sprite();
            sprite.LoadImage("bullet");
            //Components
            renderComponent = new BasicRenderComponent(sprite, Position);
            AddComponent(renderComponent);
            movementComponent = new MovementComponent(Position, Velocity, Acceleration);
            AddComponent(movementComponent);
        }
    }
}
