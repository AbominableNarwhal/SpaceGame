﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Utilities;
using Microsoft.Xna.Framework;

namespace SpaceGame
{
    public class GravityMovementComponent : MovementComponent
    {
        public static int UpGravityLevel;
        public static int DownGravityLevel;
        InteractionScheme gameObject;
        Vector2D jetPush;
        public GravityMovementComponent(InteractionScheme gameObject, Vector2D position, Vector2D velocity,
            Vector2D acceleration, Vector2D jetPush) : base(position, velocity, acceleration)
        {
            UpGravityLevel = 4000;
            DownGravityLevel = 4000;
            this.gameObject = gameObject;
            this.jetPush = jetPush;
        }
        public override void Animate(GameTime gameTime)
        {
            float secondFraction = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
            if (!gameObject.IsOnground())
            {
                Acceloration.vector.X = jetPush.vector.X;
                if (Velocity.vector.Y < 0)
                {
                    Acceloration.vector.Y = UpGravityLevel + jetPush.vector.Y;
                }
                else
                {
                    Acceloration.vector.Y = DownGravityLevel + jetPush.vector.Y;
                }
            }
            else
            {
                Acceloration.vector.Y = 0;
            }
            int maxSpeed = 2000;
            if (Math.Abs(Velocity.vector.Y + Acceloration.vector.Y*secondFraction) > maxSpeed)
            {
                Acceloration.vector.Y = 0;
                Velocity.vector.Y = (Velocity.vector.Y > 0) ? maxSpeed : -maxSpeed;
            }
            base.Animate(gameTime);
        }
    }
}
