﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Components;
using Microsoft.Xna.Framework.Input;

namespace SpaceGame
{
    class PlayerInputComponent : InputComponent
    {
        InteractionScheme gameObject;
        bool leftPressed;
        bool rightPressed;
        bool upPressed;
        public PlayerInputComponent(InteractionScheme gameObject)
        {
            this.gameObject = gameObject;
            leftPressed = false;
            rightPressed = false;
            upPressed = false;
        }
        public override void InputStateUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            if (keyboardState.IsKeyUp(Keys.A) && keyboardState.IsKeyUp(Keys.D))
            {
                gameObject.stopHorizantal();
                leftPressed = false;
                rightPressed = false;
            }
            else
            {
                if (keyboardState.IsKeyDown(Keys.A))
                {
                    if (!leftPressed)
                    {
                        leftPressed = true;
                        gameObject.MoveLeft();
                    }
                }
                else if (keyboardState.IsKeyUp(Keys.A))
                {
                    if (leftPressed)
                    {
                        leftPressed = false;
                        if (rightPressed)
                        {
                            gameObject.MoveRight();
                        }
                    }
                }
                if (keyboardState.IsKeyDown(Keys.D))
                {
                    if (!rightPressed)
                    {
                        rightPressed = true;
                        gameObject.MoveRight();
                    }
                }
                else if (keyboardState.IsKeyUp(Keys.D))
                {
                    if (rightPressed)
                    {
                        rightPressed = false;
                        if (leftPressed)
                        {
                            gameObject.MoveLeft();
                        }
                    }
                }
            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                if (!upPressed)
                {
                    upPressed = true;
                    gameObject.Ascend();
                }
            }
            else if (keyboardState.IsKeyUp(Keys.W))
            {
                if (upPressed)
                {
                    upPressed = false;
                    gameObject.AscendStop();
                }
            }
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                gameObject.Shoot();
            }
        }
    }
}
